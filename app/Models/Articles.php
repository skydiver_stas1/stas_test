<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';

    protected $fillable = [
        'category_id' ,
        'user_id',
        'title',
        'text',
        'created_at',
        'updated_at',

    ];

    public static function getAllArticles($category_id){
        return self::latest('created_at')->where('category_id', $category_id)->get();
    }

    public function comments(){
        return $this->hasMany(Comments::class, 'articles_id','id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getArticle($id) {
        return self::where('id', $id)->with('comments')->whereHas('comments', function($query) use($id){
            $query->where('articles_id', $id);
        })
        ->get();
    }
}
