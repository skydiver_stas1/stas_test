<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'parent_id' ,
        'title',
        'created_at',
        'updated_at',

    ];

    public static function getAllCategories(){
        return self::latest('created_at')->get();
    }
}
