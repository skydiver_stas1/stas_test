<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'user_id' ,
        'articles_id',
        'status',
        'text',
        'created_at',
        'updated_at',

    ];

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
