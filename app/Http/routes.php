<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home']);
Route::get('test', ['as' => 'test', 'uses' => 'TestController@index']);
Route::get('category', ['as' => 'category', 'uses' => 'CategoryController@categories']);
Route::get('article/{id}', ['as' => 'article', 'uses' => 'ArticleController@article']);
Route::get('articles/{id}', ['as' => 'articles', 'uses' => 'ArticleController@articles']);

