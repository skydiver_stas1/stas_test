<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Http\Requests;

class CategoryController extends Controller
{
    public function categories()
    {
        $allCategories = Categories::getAllCategories();
        return view('category.categories', ['allCategories' => $allCategories]);
    }
}
