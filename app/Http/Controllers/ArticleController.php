<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articles;
use App\Http\Requests;

class ArticleController extends Controller
{
    public function articles($category_id){

        $allArticles = Articles::getAllArticles($category_id);
        return view('article.articles', ['allArticles' => $allArticles]);
    }

    public function article($id) {

       // $article = Articles::getArticle($id);
        $article = Articles::getArticle($id);
//        dd(Articles::find(595)->user->id);
        return view('article.article', ['article' => $article]);
    }
}
