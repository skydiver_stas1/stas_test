@extends('app')

@section('content')
    <div class="container">
        <div class="content">
            <h3>All categories</h3>
            <ul>
                @foreach ($allCategories as $category)
                    <li><a href="{{ route('articles',['id' => $category->id]) }}">{{$category->title}}</a</li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
