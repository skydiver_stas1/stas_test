@extends('app')

@section('content')
    <div class="container">
        <div class="content">
            @foreach($allArticles as $article)
                <li><a href="{{ route('article',['id' => $article->id]) }}">{{$article->title}}</a</li>
            @endforeach
        </div>
    </div>
@stop