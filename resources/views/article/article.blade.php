@extends('app')

@section('content')
    <div class="container">
        <div class="content">
            <p>
             <p>   Author : {{$article[0]->user->name}}</p>
            <h3>{{$article[0]->title}}</h3>
            <p>{{$article[0]->text}}</p>

            <div class="col-md-6">
                @foreach($article as $comments)

                    @foreach($comments->comments as $comment)
                        <div class="col-md-8">

                            Author : {{ $comment->user->name }}

                            <div class="col-md-10"> <p>{{$comment->text}}</p></div>
                        </div>
                    @endforeach
                @endforeach
            </div>

        </div>
    </div>
@stop