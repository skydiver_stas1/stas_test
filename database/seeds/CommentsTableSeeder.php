<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 721; $i++) {
            $count = $faker->numberBetween($min = 2, $max = 20);
            for ($j = 0; $j <= $count; $j++) {

                DB::table('comments')->insert([
                    'user_id' => $faker->numberBetween($min = 1, $max = 50),
                    'articles_id' => $i,
                    'status' => 1,
                    'text' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    ]);
            }
        }
    }
}
