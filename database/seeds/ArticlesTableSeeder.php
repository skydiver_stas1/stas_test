<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($j = 0; $j < 16; $j++ ) {
            for ($i = 0; $i < 45; $i++) {
                DB::table('articles')->insert([
                    'category_id' => $j,
                    'user_id' => $faker->numberBetween($min = 1, $max = 50),
                    'title' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                    'text' => $faker->paragraph($nbSentences = 15, $variableNbSentences = true),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                ]);
            }
        }
    }
}
