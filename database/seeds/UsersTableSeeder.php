<?php


use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 0; $i < 50; $i++) {

            DB::table('users')->insert([
                'name' => $faker->userName,
                'email' => $faker->freeEmail,
                'password' => \Illuminate\Support\Facades\Hash::make($faker->password),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
